# incident-hooks

This project deploys a Google Cloud Function for handling incoming webhooks from GitLab.com for incident management purposes.

# How to Deploy

Setup a file with the name `.env.yaml` containing the secrets:

```
# The token of a GitHub user with admin access (to add hooks) to the `gitlab-com/production` repository
GITLAB_TOKEN: "XXXXX"

# The shared webhook secret, available at https://gitlab.com/gitlab-com/production/settings/integrations
WEBHOOK_TOKEN: "XXXX"

# Choose any secure value for this secret. Used to register the webhooks
REGISTER_TOKEN: "XXXXX"

# This hook URL can be obtained at https://gitlab.slack.com/services/408314798151?updated=1
NEW_ISSUE_SLACK_WEBHOOK_URL: https://hooks.slack.com/services/xxxxx/xxxx/xxxxxx
```

Once this is setup, deploy the application as follows:

```
make
```

This will deploy the scripts as Google Cloud Functions, as well as handle the registration of the webhooks on GitLab.com.



