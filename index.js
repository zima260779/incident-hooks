"use strict";

const { IncomingWebhook } = require("@slack/client");

const cloudHooks = require("./lib/cloud-hooks");

function sendWebhookForPayload(text, payload) {
  const webhook = new IncomingWebhook(process.env.NEW_ISSUE_SLACK_WEBHOOK_URL);

  let fields = [];
  if (payload.labels && payload.labels.length) {
    fields.push({
      title: payload.labels.length == 1 ? "Label" : "Labels",
      value: payload.labels.map(label => label.title).join(", "),
      short: false
    });
  }

  return webhook.send({
    text: text,
    attachments: [
      {
        fallback: payload.object_attributes.title,
        color: "#36a64f",
        author_name: payload.user.name,
        author_icon: payload.user.avatar_url,
        title: `#${payload.object_attributes.iid}: ${payload.object_attributes.title}`,
        title_link: payload.object_attributes.url,
        text: payload.object_attributes.description,
        fields: fields
      }
    ]
  });
}

module.exports = cloudHooks(
  {
    project: "gitlab-com/production",
    webhookToken: process.env.WEBHOOK_TOKEN,
    gitlabToken: process.env.GITLAB_TOKEN,
    registrationToken: process.env.REGISTER_TOKEN
  },
  events => {
    events.issue.on("open", async function(payload) {
      // Only send hooks on `incident` labelled issues
      if (!payload.labels || !payload.labels.some(label => label.title === "incident")) {
        console.log("Ignoring issue as it is not labelled as an incident");
        return;
      }

      return sendWebhookForPayload("New incident reported", payload);
    });

    events.issue.on("update", async function(payload) {
      if (!payload.changes || !payload.changes.labels) {
        // We're looking for label changes
        return;
      }

      let previousIncident = payload.changes.labels.previous.some(label => label.title === "incident");
      let currentIncident = payload.changes.labels.current.some(label => label.title === "incident");

      if (!previousIncident && currentIncident) {
        return sendWebhookForPayload("An issue has been marked as an incident", payload);
      }
    });
  }
);
