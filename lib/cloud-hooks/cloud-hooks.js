"use strict";

const Events = require("./events");

function wrap(res, inner) {
  return Promise.resolve()
    .then(() => {
      return inner();
    })
    .then(() => {
      res.status(200).end();
    })
    .catch(err => {
      console.error(err.stack || err);

      res.status(err.code || 500).send(err.message);
    });
}

class CloudHooks {
  constructor(config) {
    this.config = config;
    this.events = new Events(config);
  }

  incoming(req, res) {
    return wrap(res, () => {
      if (req.method !== "POST") {
        let err = new Error("No handler");
        err.code = 404;
        throw err;
      }

      if (req.headers["x-gitlab-token"] !== this.config.webhookToken) {
        let err = new Error("Webhook secret required");
        err.code = 401;
        throw err;
      }

      return this.events.incoming(req.headers["x-gitlab-event"], req.body);
    });
  }

  register(req, res) {
    return wrap(res, () => {
      if (req.method !== "POST") {
        let err = new Error("No handler");
        err.code = 404;
        throw err;
      }

      if (!req.body || req.body.token !== this.config.registrationToken) {
        let err = new Error("Token required");
        err.code = 401;
        throw err;
      }

      return this.events.register();
    });
  }
}

module.exports = CloudHooks;
