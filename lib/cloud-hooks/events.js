"use strict";

const gitlabFetch = require("./gitlab-fetch");

function getSelfURL() {
  if (!process.env.FUNCTION_REGION || !process.env.GCP_PROJECT) {
    throw new Error("GCP variables unset");
  }

  return `https://${process.env.FUNCTION_REGION}-${process.env.GCP_PROJECT}.cloudfunctions.net`;
}

class EventHook {
  constructor(name, header, eventFor) {
    this.name = name;
    this.header = header;
    this.handlers = {};
    this.eventFor = eventFor;
  }

  on(eventName, fn) {
    if (!this.handlers[eventName]) {
      this.handlers[eventName] = [fn];
    } else {
      this.handlers[eventName].push(fn);
    }
  }

  invoke(payload) {
    let eventName = this.eventFor(payload);
    if (!eventName) {
      console.log(`Unable to extract event name from payload, ignoring ${JSON.stringify(payload)}`);
      return Promise.resolve([]);
    }

    if (!this.handlers[eventName]) {
      console.log(`Resolved event ${eventName} for incoming payload with 0 listeners`);
      return Promise.resolve([]);
    }

    console.log(`Resolved event ${eventName} for incoming payload with ${this.handlers[eventName].length} listeners`);

    return Promise.all(
      this.handlers[eventName].map(fn => {
        return Promise.resolve(null).then(() => {
          return fn(payload);
        });
      })
    );
  }

  hasListeners() {
    return Object.keys(this.handlers).length > 0;
  }
}

function eventTypeForCommentPayload(payload) {
  switch (payload.object_attributes.noteable_type) {
    case "Commit":
      return "commit";
    case "MergeRequest":
      return "merge_request";
    case "Issue":
      return "issue";
    case "Snippet":
      return "snippet";
  }
}

class Events {
  constructor(config) {
    this.config = config;

    this.fetch = new gitlabFetch(config);

    let hooks = (this.hooks = {
      push: new EventHook("push_events", "Push Hook", () => "push"),
      tagPush: new EventHook("tag_push_events", "Tag Push Hook", () => "push"),
      issue: new EventHook("issues_events", "Issue Hook", payload => payload.object_attributes.action),
      comment: new EventHook("note_events", "Note Hook", eventTypeForCommentPayload),
      mergeRequest: new EventHook("merge_requests_events", "Merge Request Hook"),
      wikiPage: new EventHook("wiki_page_events", "Wiki Page Hook"),
      pipeline: new EventHook("pipeline_events", "Pipeline Hook"),
      build: new EventHook("job_events", "Build Hook")
    });

    this.hookLookup = Object.keys(hooks).reduce((memo, index) => {
      let value = hooks[index];
      memo[value.header] = value;
      return memo;
    }, {});
  }

  getRegisteredTypes() {
    return Object.keys(this.hooks).reduce((memo, index) => {
      let value = this.hooks[index];
      memo[value.name] = value.hasListeners();
      return memo;
    }, {});
  }

  async incoming(header, payload) {
    console.log(`Incoming webhook: ${header}`);

    let hook = this.hookLookup[header];
    if (!hook) {
      console.log(`No hooks registered for header ${header}, skipping`);
      return;
    }

    return hook.invoke(payload);
  }

  async register() {
    let selfUrl = getSelfURL();
    let projectId = encodeURIComponent(this.config.project);

    let hooksResponse = await this.fetch(`/api/v4/projects/${projectId}/hooks`);
    let json = await hooksResponse.json();

    for (let hook of json) {
      if (hook.url.indexOf(selfUrl) === 0) {
        await this.fetch(`/api/v4/projects/${projectId}/hooks/${hook.id}`, { method: "DELETE" });
      }
    }

    let creationBody = {
      url: `${selfUrl}/incoming`,
      token: process.env.WEBHOOK_TOKEN
    };
    let eventTypes = this.getRegisteredTypes();
    eventTypes.confidential_issues_events = eventTypes.issues_events;
    Object.assign(creationBody, eventTypes);

    console.log(`Sending registration with ${JSON.stringify(creationBody)}`);

    await this.fetch(`/api/v4/projects/${projectId}/hooks`, { method: "POST", body: creationBody });
  }
}

module.exports = Events;
