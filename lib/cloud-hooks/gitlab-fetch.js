"use strict";

const url = require("url");
const fetch = require("node-fetch");

// Like fetch, but for GitLab API
function gitlabFetch(config) {
  return function(uri, options) {
    let parsed;

    if (typeof uri === "string") {
      parsed = url.parse(uri, true);
    } else {
      parsed = uri;
    }

    parsed.protocol = "https";
    parsed.hostname = "gitlab.com";

    if (!options) {
      options = {};
    }

    if (!options.headers) {
      options.headers = {};
    }

    options.headers["Private-Token"] = config.gitlabToken;
    options.headers["Accept"] = "application/json";

    if (options.body && typeof options.body === "object") {
      options.headers["Content-Type"] = "application/json";
      options.body = JSON.stringify(options.body);
    }

    let u = url.format(parsed);
    console.error(`# ${u}`);

    return fetch(u, options).then(res => {
      if (res.status >= 400) {
        return res
          .json()
          .then(errorBody => {
            throw new Error(`HTTP Status ${res.status} on ${options.method || "GET"} ${u}: ${errorBody.error || JSON.stringify(errorBody)}`);
          })
          .catch(err => {
            console.log(err);
            throw new Error(`HTTP Status ${res.status} on ${options.method || "GET"} ${u}`);
          });
      }

      return res;
    });
  };
}

module.exports = gitlabFetch;
