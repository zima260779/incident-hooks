"use strict";

const CloudHooks = require("./cloud-hooks");

function cloudHooks(config, registration) {
  let instance = new CloudHooks(config);
  registration(instance.events.hooks);

  return {
    incoming: instance.incoming.bind(instance),
    register: instance.register.bind(instance)
  };
}

module.exports = cloudHooks;
